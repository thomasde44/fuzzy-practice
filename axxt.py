import torch


def update(A, x):
    # A = A - x * numpy.transpose(x)
    A = A + x * torch.transpose(x, 0, 1)
    return A

def update2(A, x):
    # A = A - x * numpy.transpose(x)
    A = A - x * torch.transpose(x, 0, 1)
    return A

def float_to_bipolar(x):
    return torch.floor(2 * x - 1)

A = torch.zeros(size=(10, 10), dtype=torch.float32)


x = torch.rand(size=(10, 1), dtype=torch.float32)

y = float_to_bipolar(x)

print(x * torch.transpose(x, 0, 1))


V_data = [-1, 1, 1]
X_data = [1, -1, -1, 1]

V = torch.tensor(V_data)
X = torch.tensor(X_data)
print(X)
print(V * torch.transpose(X, 0, -1))
# print(X)


# for i in range(100):
#     A = update(A, x)
#     # print(A)

# for i, x in enumerate(A):
#     for j, y in enumerate(x):
#         print(y)
#     print("new row")
