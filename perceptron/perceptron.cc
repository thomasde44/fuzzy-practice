#include <stdio.h>
#include "perceptron.h"
#include "stdlib.h"


inneuron::inneuron(float j) {
    weight = j;
}

float inneuron::act(float x) {
    float a;
    a = x * weight;
    return a;
}

void outneuron::activate(float * inputv, inneuron * nrn) {
    int i;
    activation = 0;

    for (i = 0; i < 4; i++) {
        std::cout << "\n weight for neuron " << i+1 << " is  " << nrn[i].weight;
        nrn[i].activation = nrn[i].act(inputv[i]);

        std::cout << "   activation is    " << nrn[i].activation;
        activation += nrn[i].activation;
    }
    std::cout << "\n\n activation is " << activation << "\n";
} 

int outneuron::outvalue(float j) {
    if (activation >= j) {
        std::cout << "\n the output neuron activation exceeds the threshold value " << j << "\n";
        output = 1;
    } else {
        std::cout  << "\n the output neuron activation is smaller than the threshold value " << j << "\n";
        output = 0;
    }
    std::cout << " output value is " << output;
    return (output);
}


network::network(float a, float b, float c, float d) {
    nrn[0] = inneuron(a);
    nrn[1] = inneuron(b);
    nrn[2] = inneuron(c);
    nrn[3] = inneuron(d);
    onrn = outneuron();
    onrn.activation = 0;
    onrn.output = 0;
}

int main(int argc, char * argv[]) {
    float inputv1[] = {1.95, 0.27, 0.69, 1.25};
    float wtv1[] = {2, 3, 3, 2};
    float wtv2[] = {3, 0, 6, 2};

    FILE * wfile;
    FILE * infile;
    int num = 0;
    int vecnum = 0;
    int i;
    float threshold = 7.0;

    if (argc < 2) {
        std::cerr << "usage: a.out weightfile inputfile";
        exit(1);
    }
    // open files
    wfile = fopen(argv[1], "r");
    infile = fopen(argv[2], "r");

    if ((wfile == NULL) || (infile == NULL)) {
        std::cout << "error opening file\n";
        exit(1);
    }

    std::cout << "\n This program is for a perceptron network with an input layer of 4 neurons, each connected to the output neuron.\n";
    std::cout << "\n This example takes real numbers as input signals\n";

    // create the network by calling the constructor
    // the constructor calls the neuron constructor as many times as the number of nerons in the input layer

    std::cout << "\n please enter the number of weights/vectors \n";
    std::cin >> vecnum;

    for (i = 0; i < vecnum; i++) {
        fscanf(wfile, "%f %f %f %f\n", &wtv1[0], &wtv1[1], &wtv1[2], &wtv1[3]);
        network h1(wtv1[0], wtv1[1], wtv1[2], wtv1[3]);
        fscanf(infile, "%f %f %f %f\n", &inputv1[0], &inputv1[1], &inputv1[2], &inputv1[3]);

        std::cout << "this is vector # " << i << "\n";
        std::cout << "please enter a threshold value, eq 7.0\n";
        std::cin >> threshold;
        h1.onrn.activate(inputv1, h1.nrn);
        h1.onrn.outvalue(threshold);
        std::cout << "\n\n";
    }
    fclose(wfile);
    fclose(infile);
}