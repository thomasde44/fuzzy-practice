#include <stdio.h>
#include <iostream>
#include <math.h>

class inneuron {
    protected:
        float weight;
        float activation;
        friend class outneuron;
    public:
        inneuron() {};
        inneuron(float j);
        float act(float x);
};

class outneuron {
    protected:
        int output;
        float activation;
        friend class network;
    public:
        outneuron() {};
        void activate(float x[4], inneuron * nrn);
        int outvalue(float j);
};

class network {
    public:
        inneuron nrn[4];
        outneuron onrn;
        network(float, float, float, float);
};