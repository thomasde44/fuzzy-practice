 g++ *.h *.cc<br />
 
 
 This program is for a perceptron network with an input layer of 4 neurons, each connected to the output neuron.<br />

 This example takes real numbers as input signals<br />

 please enter the number of weights/vectors <br />
3<br />
this is vector # 0 <br />s
please enter a threshold value, eq 7.0 <br />
5.0<br />

 weight for neuron 1 is  2   activation is    3.9<br />
 weight for neuron 2 is  3   activation is    0.81<br />
 weight for neuron 3 is  3   activation is    2.07<br />
 weight for neuron 4 is  2   activation is    2.5<br />

 activation is 9.28<br />

 the output neuron activation exceeds the threshold value 5<br />
 output value is 1<br />

this is vector # 1<br />
please enter a threshold value, eq 7.0<br />
5.0<br />

 weight for neuron 1 is  3   activation is    0.9<br />
 weight for neuron 2 is  0   activation is    0<br />
 weight for neuron 3 is  6   activation is    4.5<br />
 weight for neuron 4 is  2   activation is    0.38<br />

 activation is 5.78<br />

 the output neuron activation exceeds the threshold value 5<br />
 output value is 1<br />

this is vector # 2<br />
please enter a threshold value, eq 7.0<br />
5.0 <br />

 weight for neuron 1 is  2   activation is    2.8<br />
 weight for neuron 2 is  6   activation is    3.6<br />
 weight for neuron 3 is  8   activation is    2.8<br />
 weight for neuron 4 is  3   activation is    2.97<br />

 activation is 12.17<br />

 the output neuron activation exceeds the threshold value 5<br />
 output value is 1<br />

