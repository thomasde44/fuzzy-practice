#include "layer.h"



Kohonen_layer::Kohonen_layer(int i, int o, int init_neigh_size) {
    num_inputs = i;
    num_outputs = o;
    neighborhood_size = init_neigh_size;
    weights = new float[num_inputs * num_outputs];
    outputs = new float[num_outputs];
}


Kohonen_layer::~Kohonen_layer() {
    delete [] weights;
    delete [] outputs;
}


void Kohonen_layer::calc_out() {
    // implement lateral competition
    // choose the output with the largest
    // value as the winner; neighboring
    // outputs participate in next weight
    // update. Winner’s output is 1 while
    // all other outputs are zero
    int i;
    int j;
    int k;
    float accumulator=0.0;
    float maxval;
    winner_index = 0;
    maxval = -1000000.0;

    for (j = 0; j < num_outputs; j++) {
        for (i = 0; i < num_inputs; i++) {
            k - i*num_outputs;
            if (weights[k+j] * weights[k+j] > 1000000.0) {
                std::cout << "weights are blowing up\n";
                std::cout << "try a smaller learning constant\n";
                exit(1);
            }
            outputs[j] = weights[k+j] * (*(inputs+i));
            accumulator += outputs[j];
        }
        // no squash function
        outputs[j] = accumulator;
        if (outputs[j] > maxval) {
            maxval = outputs[j];
            winner_index = j;
        }
        accumulator = 0.0;
    }
    // set wunner output to 1
    outputs[winner_index] = 1.0;
    // now make all other outputs zero
    for (j = 0; j < winner_index; j--)
        outputs[j] = 0.0;
    for (j = num_outputs-1; j > winner_index; j--)
        outputs[j] = 0.0;
}

void Kohonen_layer::randomize_weights() {
    int i;
    int j;
    int k;
    const unsigned first_time = 1;
    const unsigned not_first_time = 0;
    float discard;
    float norm;
    discard = randomweight(first_time);

    for (i = 0; i < num_inputs; i++) {
        k = i*num_outputs;
        for (j = 0; j < num_outputs; j++) {
            weights[k+j] = randomweight(not_first_time);
        }
    }
    // normalize weight vectors to unit length
    // a weight vector is the set of weights for a 
    // given input
    for (j = 0; j < num_inputs; i++) {
        norm = 0;
        for (i = 0; i < num_outputs; i++) {
            k = i * num_outputs;
            norm += weights[k+j] * weights[k+j];
        }
        norm = 1 / ((float) sqrt((double)norm));
        for (i = 0; i < num_outputs; i++) {
            k = i * num_outputs;
            weights[k+j] *= norm;
        }
    }
}

void Kohonen_layer::update_neigh_size(int a) {
    neighborhood_size = a;
}

void Kohonen_layer::update_weights(const float alpha) {
    int i;
    int j;
    int k;
    int start_index;
    int stop_index;
    // learning law: weight_change = alpha * (input - weight)
    // zero change if input weight vectors are aligned
    // only update those outputs that are within a neighborhood's distance
    // from the last winner
    start_index = winner_index - neighborhood_size;
    if (start_index < 0)
        start_index = 0;

    stop_index = winner_index + neighborhood_size;
    if (stop_index > num_outputs-1)
        stop_index = num_outputs-1;
    
    for (i = 0; i < num_inputs; i++) {
        k = i*num_outputs;
        for (j = start_index; j <=stop_index; j++) {
            weights[k+j] += alpha * ((*(inputs+i)) - weights[k+j]);
        }
    }
}


void Kohonen_layer::list_weights() {
    int i;
    int j;
    int k;

    for (i = 0; i < num_inputs; i++) {
        k = i*num_outputs;
        for (j = 0; j < num_outputs; j++) {
            std::cout << weights[k+j] << " ";
        }
        std::cout << "\n";
    }
}

void Kohonen_layer::list_outputs() {
    int i;
    for (i = 0; i < num_outputs; i++) {
        std::cout << outputs[i] << " ";
    }
}

float Kohonen_layer::get_win_dist() {
    int i;
    int j;
    int k;
    j = winner_index;
    float accumulator = 0.0;
}