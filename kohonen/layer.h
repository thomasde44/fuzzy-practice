#include <iostream>
#include <math.h>
#include <time.h>
#include <stdlib.h>
#include <stdio.h>
#define MAX_LAYERS 5
#define MAX_VECTORS 100

inline float squash(float input) {
    // squashing fn
    // use sigmoid
    if (input < -50) {
        return 0.0;
    } else if (input > 50) {
        return 1.0;
    } else {
        return (float) (1 / (1 + exp(-(double)input)));
    }
}

inline float randomweight(unsigned int init) {
    int num;
    // random number generator 
    // returns a random number between -1 and 1
    if (init == 1) {
        srand((unsigned) time(NULL));
    }
    num = rand() % 100;
    return 2 * ((float) num / 100.0) - 1.0;
}

class network;

class Kohonen_network;

class layer {
    protected:
        int num_inputs;
        int num_outputs;
        float * outputs;
        float * inputs;

        friend network;
        friend Kohonen_network;
    public:
        virtual void calc_out() = 0;
};

class Kohonen_layer: public layer {
    protected:
        float * weights;
        int winner_index;
        float win_distance;
        int neighborhood_size;
    public:
        Kohonen_layer(int i, int o, int init_neigh_size);
        ~Kohonen_layer();
        virtual void calc_out();
        void randomize_weights();
        void update_neigh_size(int a);
        void update_weights(const float a);
        void list_weights();
        void list_outputs();
        float get_win_dist();
};

class Kohonen_network {
    private:
        layer * layer_ptr[2];
        int layer_size[2];
        int neighborhood_size;
    public:
        Kohonen_netowrk();
        ~Kohonen_network();
        void get_layer_info();
        void set_up_network(int a);
        void get_layer_info();
        void set_up_network(int a);
        void randomize_weights();
        void update_neigh_size(int a);
        void update_weights(const float a);
        void list_weights();
        void list_outputs();
        void get_next_vector(FILE * fp);
        void process_next_pattern();
        float get_win_dist();
        int get_win_index();
};