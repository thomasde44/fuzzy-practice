#include "hop.h"


// constructor for the neuron class
neuron::neuron(int * j) {
    int i;
    for(i = 0; i < 4; i++) {
        weightv[i] = * (j+i);
    }
}

// take the dot product of the input vector and the weight vector
int neuron::act(int m, int * x) {
    int i;
    int a = 0;

    for (i = 0; i < m; i++) {
        a += x[i] * weightv[i];
    }
    return a;
}

int network::threshold(int k) {
    
    if (k >= 0) {
        return (1);
    } else {
        return (0);
    }
}

network::network(int a[4], int b[4], int c[4], int d[4]) {
    nrn[0] = neuron(a);
    nrn[1] = neuron(b);
    nrn[2] = neuron(c);
    nrn[3] = neuron(d);
}

void network::activation(int * pattern) {
    int i;
    int j;

    for (i = 0; i < 4; i++) {
        for (j = 0; j < 4; j++){
            std::cout << "\n nrn["<<i<<"].weightv["<<j<<"] is " << nrn[i].weightv[j];
        }
        nrn[i].activation = nrn[i].act(4, pattern);
        std::cout << "\n activation is " << nrn[i].activation;
        output[i] = threshold(nrn[i].activation);
        std::cout << "\n output is " << output[i] << "\n";
    }
}

int main() {
    int pattern1[] = {1, 0, 1, 0};
    int i;
    int wt1[] = {0, -5, 4, 4};
    int wt2[] = {-5, 0, 4, 4};
    int wt3[] = {4, 4, 0, -5};
    int wt4[] = {4, 4, -5, 0};

    std::cout << "\n this program is for a hopfield netowrk with a single layer of 4 fully connected neurons, the netork is made to recall the patterns 1010 and 0101";
    
    // create the network here calling its contstructor
    network h1(wt1, wt2, wt3, wt4);

    // present a pattern to the network and have the activations of the neurons
    h1.activation(pattern1);

    // check if the input pattern is correctly recalled
    for (i = 0; i < 4; i++) {
        if (h1.output[i] == pattern1[i]) {
            std::cout << "\n pattern = " << pattern1[i] << " output = " << h1.output[i] << " match";
        } else {
            std::cout << "\n pattern = " << pattern1[i] << " output = " << h1.output[i] << " no match";
        }
    }
    
    std::cout << "\n\n";
    int pattern2[] = {0, 1, 0, 1};
    h1.activation(pattern2);

    for (i = 0; i < 4; i++) {
        if (h1.output[i] == pattern2[i]) {
            std::cout << "\n pattern = " << pattern2[i] << " output = " << h1.output[i] << " component matches";
        } else {
            std::cout << "\n pattern = " << pattern2[i] << " output = " << h1.output[i] << " component does not match";
        }
    }
    

}   
