compile<br />

g++ *.h *.cc<br />



example to make correlation matrices to remeber the input pattern<br />

a = [1 0 1 0] <br />
b = [0 1 0 1]<br />

step 1:<br />
bipolar <br />

a -> [1 -1 1 -1]<br />
b -> [-1 1 -1 1]<br />

transpose multiply each vector and subtract identity matrix<br />

m = (a.a' - eye(4)) + (b.b' - eye(4))<br />

./a.out <br />

 this program is for a hopfield netowrk with a single layer of 4 fully connected neurons, the netork is made to recall the <br />patterns 1010 and 0101<br />
 nrn[0].weightv[0] is 0<br />
 nrn[0].weightv[1] is -3<br />
 nrn[0].weightv[2] is 3<br />
 nrn[0].weightv[3] is -3<br />
 activation is 3<br />
 output is 1<br />

 nrn[1].weightv[0] is -3<br />
 nrn[1].weightv[1] is 0<br />
 nrn[1].weightv[2] is -3<br />
 nrn[1].weightv[3] is 3<br />
 activation is -6<br />
 output is 0<br />

 nrn[2].weightv[0] is 3<br />
 nrn[2].weightv[1] is -3<br />
 nrn[2].weightv[2] is 0<br />
 nrn[2].weightv[3] is -3<br />
 activation is 3<br />
 output is 1<br />

 nrn[3].weightv[0] is -3<br />
 nrn[3].weightv[1] is 3<br />
 nrn[3].weightv[2] is -3<br />
 nrn[3].weightv[3] is 0<br />
 activation is -6<br />
 output is 0<br />

 pattern = 1 output = 1 match<br />
 pattern = 0 output = 0 match<br />
 pattern = 1 output = 1 match<br />
 pattern = 0 output = 0 match<br />


 nrn[0].weightv[0] is 0<br />
 nrn[0].weightv[1] is -3<br />
 nrn[0].weightv[2] is 3<br />
 nrn[0].weightv[3] is -3<br />
 activation is -6<br />
 output is 0<br />

 nrn[1].weightv[0] is -3<br />
 nrn[1].weightv[1] is 0<br />
 nrn[1].weightv[2] is -3<br />
 nrn[1].weightv[3] is 3<br />
 activation is 3<br />
 output is 1<br />

 nrn[2].weightv[0] is 3<br />
 nrn[2].weightv[1] is -3<br />
 nrn[2].weightv[2] is 0<br />
 nrn[2].weightv[3] is -3<br />
 activation is -6<br />
 output is 0<br />

 nrn[3].weightv[0] is -3<br />
 nrn[3].weightv[1] is 3<br />
 nrn[3].weightv[2] is -3<br />
 nrn[3].weightv[3] is 0<br />
 activation is 3<br />
 output is 1<br />

 pattern = 0 output = 0 component matches<br />
 pattern = 1 output = 1 component matches<br />
 pattern = 0 output = 0 component matches<br />
 pattern = 1 output = 1 component matches<br />