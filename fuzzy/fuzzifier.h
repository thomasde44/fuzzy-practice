


class category 
{
    private:
        char name[30];
        float lowval;
        float highval;
        float midval;

    public:
        category(){};
        void setname(char *);
        char * getname();
        void setval(float&, float&, float&);
        float getlowval();
        float gethighval();

        float getshare(const float&);

        ~category(){};
};

int randnum(int);