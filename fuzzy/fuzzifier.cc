
#include <iostream>
#include <stdlib.h>

#include <time.h>
#include <string.h>
#include "fuzzifier.h"


void category::setname(char *n)
{
    strcpy(name, n);
}

char * category::getname()
{
    return name;
}

void category::setval(float &h, float &m, float &l)
{
    highval = h;
    midval = m;
    lowval = l;
}

float category::getlowval()
{
    return lowval;
}

float category::gethighval()
{
    return highval;
}

float category::getshare(const float & input) 
{
    // relative membership of an input in a categor
    // max 1.0
    float output;
    float midlow;
    float highmid;

    midlow = midval - lowval;
    highmid = highval - midval;

    // if not in the range output 0
    // lowval is most allowable lower bound of the set
    if ((input <= lowval) || (input >= highval)) {
        output = 0;
    }
    else {
        if (input > midval) {
            output = (highval - input) / highmid;
        } else if (input == midval) {
            output = 1.0;
        } else {
            output = (input - lowval) / midlow;
        }
    }
    return output;
}

int randomnum(int maxval) 
{
    // randon number generator
    //  will return an integer up to maxval

    srand((unsigned)time(NULL));
    return rand() % maxval;
}


int main() {
    // fuzzy logic program that takes a category information:
    // lowval, midval, highval, category name
    // and applies fuzzy logic
    // and makes it fuzzy based the total number of categories
    // and membership of each category

    int i = 0;
    int j = 0;
    int numcat = 0;
    int randnum;

    float l = 0;
    float m = 0;
    float h = 0;
    float inval = 1.0;

    // float l, m, h, inval = 1.0;
    // printf("%f %f %f %f", l, m, h, inval);

    char input[30] ="              ";
    category * ptr[10];
    float relprob[10];
    float total = 0;
    float runtotal = 0;

    while (1) {
        std::cout << "Enter category name: ex. Cool\n";
        std::cout << "Enter one word without spaces\n";
        std::cout << "When you are done type done\n\n";

        ptr[i] = new category;
        std::cin >> input;
        // check if input is == done
        if ((input[0] == 'd' && input[1] == 'o' && input[2] == 'n' && input[3] == 'e')) {
            break;
        }

        ptr[i] -> setname(input);

        std::cout << "\nType in the lowval, midval, highval\n";
        std::cout << "for each category, separated by spaces \n";
        std::cout << "ex. 1.0 3.0 5.0 :\n\n";

        std::cin >> l >> m >> h;
        ptr[i] -> setval(h, m, l);
        i++;
    }

    numcat = i; // number of categories

    // categories set up now input the data to fuzzify
    std::cout << "=======================================\n";
    std::cout << "==============Ready for Data===============\n";
    std::cout << "=======================================\n";

    while(1) {
        std::cout << "\n input a data value, type 0 to terminate\n";
        std::cin >> inval;

        if (inval == 0) {
            break;
        }

        // calculate the relative probability of each category

        for (int j = 0; j < numcat; j++) {
            relprob[j] = 100 * ptr[j] -> getshare(inval);
            total += relprob[j];
        }

        if (total == 0) {
            std::cout << "data out of range\n";
            exit(1);
        }

        randnum = randomnum((int)total);

        j = 0;
        runtotal = relprob[0];

        while((runtotal < randnum) && (j < numcat)) {
            j++;
            runtotal += relprob[j];
        }
        std::cout << "\noutput fuzzy category is ==> " <<
            ptr[j] -> getname() << " <== \n";
            std::cout << "category\t " << "membership\n";
            std::cout << "-----------------------------\n";
        for (int j = 0; j < numcat; j++) {
            std::cout << ptr[j] -> getname() << "\t\t" << 
                (relprob[j]/total) << "\n";
        }
    }

    std::cout << "\n\nDone\n";

}