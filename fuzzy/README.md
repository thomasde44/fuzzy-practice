
compile: g++ *.h *.cc<br />

input sequence:<br />

Enter category name: ex. Cool<br />
Enter one word without spaces<br />
When you are done type done<br />

Happy<br />

Type in the lowval, midval, highval<br />
for each category, separated by spaces <br />
ex. 1.0 3.0 5.0 :<br />

0.0 5.0 10.0<br />
Enter category name: ex. Cool<br />
Enter one word without spaces<br />
When you are done type done<br />

Sad<br />

Type in the lowval, midval, highval<br />
for each category, separated by spaces <br />
ex. 1.0 3.0 5.0 :<br />

1.0 2.0 3.0<br />
Enter category name: ex. Cool<br />
Enter one word without spaces<br />
When you are done type done<br />

Crazy<br />

Type in the lowval, midval, highval<br />
for each category, separated by spaces <br />
ex. 1.0 3.0 5.0 :<br />

2.0 20.0 40.0<br />
Enter category name: ex. Cool<br />
Enter one word without spaces<br />
When you are done type done<br />

done<br />
=======================================<br />
==============Ready for Data===============<br />
=======================================<br />

 input a data value, type 0 to terminate<br />
1<br />

output fuzzy category is ==> Happy <== <br />
category	 membership<br />
-----------------------------<br />
Happy		1<br />
Sad		0<br />
Crazy		0<br />

 input a data value, type 0 to terminate<br />
2<br />

output fuzzy category is ==> Happy <== <br />
category	 membership<br />
-----------------------------<br />
Happy		0.25<br />
Sad		0.625<br />
Crazy		0<br />

 input a data value, type 0 to terminate<br />
3<br />

output fuzzy category is ==>  <== <br />
category	 membership<br />
-----------------------------<br />
Happy		0.26601<br />
Sad		0<br />
Crazy		0.0246305<br />

 input a data value, type 0 to terminate<br />
4<br />

output fuzzy category is ==> Happy <== <br />
category	 membership<br />
-----------------------------<br />
Happy		0.252632<br />
Sad		0<br />
Crazy		0.0350877<br />

ex: interpret this as with input of 4 is 25% of crazy is happy<br />

 input a data value, type 0 to terminate<br />
5<br />

output fuzzy category is ==> Crazy <== <br />
category	 membership<br />
-----------------------------<br />
Happy		0.230769<br />
Sad		0<br />
Crazy		0.0384615<br />

 input a data value, type 0 to terminate<br />
6<br />

output fuzzy category is ==>  <== <br />
category	 membership<br />
-----------------------------<br />
Happy		0.149378<br />
Sad		0<br />
Crazy		0.0414938<br />


 input a data value, type 0 to terminate<br />

7<br />

output fuzzy category is ==>  <== <br />
category	 membership<br />
-----------------------------<br />
Happy		0.0962567<br />
Sad		0<br />
Crazy		0.0445633<br />

 input a data value, type 0 to terminate<br />
8<br />

output fuzzy category is ==>  <== <br />
category	 membership<br />
-----------------------------<br />
Happy		0.0574163<br />
Sad		0<br />
Crazy		0.0478469<br />

 input a data value, type 0 to terminate<br />
50<br />

output fuzzy category is ==>  <== <br />
category	 membership<br />
-----------------------------<br />
Happy		0<br />
Sad		0<br />
Crazy		0<br />

 input a data value, type 0 to terminate<br />
3<br />

output fuzzy category is ==> Happy <==<br />
category	 membership<br />
-----------------------------<br />
Happy		0.0787172<br />
Sad		0<br />
Crazy		0.00728863<br />

 input a data value, type 0 to terminate<br />
4<br />

output fuzzy category is ==> Happy <== <br />
category	 membership<br />
-----------------------------<br />
Happy		0.09375<br />
Sad		0<br />
Crazy		0.0130208<br />

