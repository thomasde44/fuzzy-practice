#include <iostream>
#include <math.h>
#include <stdlib.h>

#define MXSIZ 10

class bmneuron {
    protected:
        int nnbr;
        int inn;
        int outn;
        int output;
        int activation;
        int outwt[MXSIZ];
        char *name;
        friend class network;

    public:
        bmneuron() {};
        void getnrn(int m1, int m2, int m3, char * y);
};


class exemplar {
    protected:
        int xdim;
        int ydim;
        int v1[MXSIZ];
        int v2[MXSIZ];
        int u1[MXSIZ];
        int u2[MXSIZ];
        friend class network;

    public:
        exemplar() {};
        void getexmplr(int k, int l, int * b1, int * b2);
        void prexmplr();
        void trnsfrm();
        void prtnsfrm();
};

class asscpair {
    protected:
        int xdim;
        int ydim;
        int idn;
        int v1[MXSIZ];
        int v2[MXSIZ];
        friend class network;
    public:
        asscpair() {};
        void getasscpair(int, int, int);
        void prasscpair();
};

class potlpair {
    protected:
        int xdim;
        int ydim;
        int v1[MXSIZ];
        int v2[MXSIZ];
        friend class network;
    public:
        potlpair() {};
        void getpotlpair(int, int);
        void prpotlpair();
};

class network {
    public:
        int anmbr;
        int bnmbr;
        int flag;
        int nexmplr;
        int nasspr;
        int ninpt;
        bmneuron (anrn)[MXSIZ];
        bmneuron (bnrn)[MXSIZ];
        exemplar (e)[MXSIZ];
        asscpair (as)[MXSIZ];
        potlpair (pp)[MXSIZ];
        int outs1[MXSIZ];
        int outs2[MXSIZ];
        int mtrx1[MXSIZ][MXSIZ];
        int mtrx2[MXSIZ][MXSIZ];
        network() {};
        void getnwk(int, int, int, int [][6], int [][5]);
        void compr1(int, int);
        void compr2(int, int);
        void prwts();
        void iterate();
        void findassc(int *);
        void asgninpt(int *);
        void asgnvect(int j1, int * b1, int * b2);
        void comput1();
        void comput2();
        void prstatus();
};