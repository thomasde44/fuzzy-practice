import pandas as pd
import numpy as np




dfsp500 = pd.read_csv('s&p500-historical-highlow.csv')


dft30 = pd.read_csv('T-bill-yield-30year.csv')


dft3 = pd.read_csv('T-bill-yield-3month.csv')

dfnyse = pd.read_csv('nyse-historical-highlow.csv')


dft30 = dft30.dropna()
dfsp500 = dfsp500.dropna()
dft3 = dft3.dropna()
dfnyse = dfnyse.dropna()

# print(['Close'])


# Filter the DataFrame for the specified date range
filtered_dft30 = dft30[(dft30['Date'] >= '1982-04-20') & (dft30['Date'] <= '2002-04-20')]

filtered_dft3 = dft3[(dft3['Date'] >= '1982-04-20') & (dft3['Date'] <= '2002-04-20')]

filtered_dfsp500 = dfsp500[(dfsp500['Date'] >= '1982-04-20') & (dfsp500['Date'] <= '2002-04-20')]

filtered_dfnyse = dfnyse[(dfnyse['Date'] >= '1982-04-20') & (dfnyse['Date'] <= '2002-04-20')]

print(filtered_dft3.reset_index(drop=True))

print(filtered_dft30.reset_index(drop=True))

print(filtered_dfsp500.reset_index(drop=True))

print(filtered_dfnyse.reset_index(drop=True))


# print(filtered_dft30['Close'])

column0 = filtered_dfsp500['Date'].reset_index(drop=True)
column1 = filtered_dft30['Close'].reset_index(drop=True)
column2 = filtered_dft3['Close'].reset_index(drop=True)
column3 = filtered_dfsp500['Close'].reset_index(drop=True)
column4 = filtered_dfnyse['Close'].reset_index(drop=True)



# print(filtered_dfsp500['Date'].reset_index(drop=True))

# # Create a new DataFrame with the extracted columns
new_df = pd.DataFrame({
    'Date': filtered_dfsp500['Date'],
    'dft30': column1,
    'dfsp500': column2,
    'dft3': column3,
    'dfnyse': column4
})
new_df = new_df.dropna()
cut_df = new_df[:-50]

res = []
# for i in new_df.iterrows():
#     print(i)

rolling_mean = new_df['dft30'].rolling(window=5, center=True).mean()

# Apply the formula
roc1 = (new_df['dft30'] - rolling_mean) / (new_df['dft30'] + rolling_mean)
roc2 = (new_df['dfsp500'] - rolling_mean) / (new_df['dfsp500'] + rolling_mean)
roc3 = (new_df['dft3'] - rolling_mean) / (new_df['dft3'] + rolling_mean)
roc4 = (new_df['dfnyse'] - rolling_mean) / (new_df['dfnyse'] + rolling_mean)

# Create a new DataFrame with the results
result_df = pd.DataFrame({
    'ROC1': roc1,
                          'ROC2': roc2,
                          'ROC3': roc3,
                          'ROC4': roc4
                        })


print(result_df.dropna())
# for i in result_df.iterrows():
#     print(i)