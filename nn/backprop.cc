// #include "layer.cc"
#include "layer.h"
// #include "layer.cc"
#include <stdio.h>
#define TRAINING_FILE "training.dat"
#define WEIGHTS_FILE "weights.dat"
#define OUTPUT_FILE "output.dat"
#define TEST_FILE "test.dat"


int main() {
    float error_tolerance = 0.1;
    float total_error = 0.0;
    float avg_error_per_cycle = 0.0;
    float error_last_cycle = 0.0;
    float avg_err_per_pattern = 0.0;
    float error_last_pattern = 0.0;
    float learning_parameter = 0.02;
    unsigned temp;
    unsigned startup;
    long int vectors_in_buffer;
    long int max_cycles;
    long int patterns_per_cycle = 0;

    long int total_cycles;
    long int total_patterns;
    int i;

    network backp;

    FILE * training_file_ptr;
    FILE * weights_file_ptr;
    FILE * output_file_ptr;

    FILE * test_file_ptr;
    FILE * data_file_ptr;

    if ((output_file_ptr = fopen(OUTPUT_FILE, "w")) == NULL) {
        std::cout << "problem opening output file\n";
        exit(1); 
    }

    std::cout << "----------------\n";
    std::cout << "Backpropagation version 1\n";
    std::cout << "enter 1 for training on or 0 for off: \n";
    std::cout << "use training to change weights according to your expected output\n";
    std::cout << "training.dat should contain the a set of inputs and expected outputs\n";
    std::cout << "the number of inputs determines the size of the first layer and the outputs determine the size of the output layer\n";

    std::cin >> temp;
    backp.set_training(temp);

    if (backp.get_training_value() == 1) {
        std::cout << "training is on\n";
        std::cout << "in the file weights.dat weights will be saved\n";
    } else {
        std::cout << "training is off\n";
        std::cout << "in the file weights.dat weights will be loaded\n";
        std::cout << "test.dat should contain only inputs and no outputs\n";
    }

    if (backp.get_training_value() == 1) {
        std::cout << "enter tolerance between 0.001 and 100.0: \n";
        std::cout << "and learning rate beta between 0.001 and 1.0: \n";
        std::cout << "ex: 0.1 0.5\n";

        // take in the values
        std::cin >> error_tolerance >> learning_parameter;
        if ((training_file_ptr = fopen(TRAINING_FILE, "r")) == NULL) {
            std::cout << "problem opening training file\n";
            exit(1);
        }
        data_file_ptr = training_file_ptr;

        std::cout << "enter max cycles for the simulation\n";
        std::cout << "a cycle is one pass through the data set\n";
        std::cin >> max_cycles;
    } else {
        if ((test_file_ptr = fopen(TEST_FILE, "r")) == NULL) {
            std::cout << "problem opening test file\n";
            exit(1);
        }
        data_file_ptr = test_file_ptr;
        }
    total_cycles = 0;
    total_patterns = 0;
    backp.get_layer_info();
    backp.set_up_network();

    if (backp.get_training_value() == 1) {
        if ((weights_file_ptr = fopen(WEIGHTS_FILE, "w")) == NULL) {
            std::cout << "problem opening weights file\n";
            exit(1);
        }
        backp.randomize_weights();
    } else {
        if ((weights_file_ptr = fopen(WEIGHTS_FILE, "r")) == NULL) {
            std::cout << "problem opening weights file\n";
            exit(1);
        }
        backp.read_weights(weights_file_ptr);
    }

    startup = 1;
    vectors_in_buffer = MAX_VECTORS;
    total_error = 0;

    while (((backp.get_training_value() == 1) && (avg_err_per_pattern
                > error_tolerance) && 
                (total_cycles < max_cycles) &&
                (vectors_in_buffer != 0)) ||
                ((backp.get_training_value() == 0) 
                && (total_cycles < 1)) || 
                ((backp.get_training_value() == 1)
                && (startup == 1))
                ) {
                    startup = 0;
                    error_last_cycle = 0; 
                    patterns_per_cycle = 0;

                while((vectors_in_buffer == MAX_VECTORS)) {
                    vectors_in_buffer = backp.fill_IObuffer(data_file_ptr);
                    if (vectors_in_buffer < 0) {
                        std::cout << "error in reading vectos aborting\n";
                        exit(1);
                    }
                    for (i = 0; i < vectors_in_buffer; i++) {
                        backp.set_up_pattern(i);
                        total_patterns++;
                        patterns_per_cycle++;
                        backp.forward_prop();

                        if (backp.get_training_value() == 0) {
                            backp.write_outputs(output_file_ptr);
                        }

                        if (backp.get_training_value() == 1) {
                            backp.backward_prop(error_last_pattern);
                            error_last_cycle += error_last_pattern * error_last_pattern;
                            backp.update_weights(learning_parameter);
                        }
                    }
                    error_last_pattern = 0;
                }
                avg_err_per_pattern=((float)sqrt((double)error_last_cycle / patterns_per_cycle));
                total_error += error_last_cycle;
                total_cycles++;

                std::cout << "\n\n\n\n\n\n\n\n";
                std::cout << total_cycles << "\t" << avg_err_per_pattern << "\n";

                fseek(data_file_ptr, 0L, SEEK_SET);

                vectors_in_buffer = MAX_VECTORS; 
            }
        if (backp.get_training_value() == 1) {
            backp.write_weights(weights_file_ptr);
            backp.write_outputs(output_file_ptr);
            avg_error_per_cycle = (float)sqrt((double) total_error / total_cycles);

            error_last_cycle = (float)sqrt((double)error_last_cycle);

            std::cout << avg_error_per_cycle << error_last_cycle << "\n";
            std::cout << avg_err_per_pattern << "\n";
        }

        fclose(data_file_ptr);
        fclose(weights_file_ptr);
        fclose(output_file_ptr);
}
